const functions = require('firebase-functions')
const admin = require('firebase-admin').initializeApp()
const firestore = admin.firestore()
const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const mailgun = require('mailgun.js')
const ejs = require('ejs')

const mg = mailgun.client({
  username: 'api',
  key: 'key-ee62841f45e6def19ff9155ca1412354',
  public_key: 'pubkey-49a571b232fd1ebfbc3afe1559875017'
})

firestore.settings({ timestampsInSnapshots: true })

app.use(cors({ origin: true }))
app.use(bodyParser.json())

app.post('/subscriptions', (req, res) => {
  const key = req.body.key
  const paymentData = req.body.token
  const plan = req.body.plan

  const stripe = require('stripe')(key)

  stripe.customers.create({
    description: `Customer for ${paymentData.email}`,
    source: paymentData.id// obtained with Stripe.js
  }, (err, customer) => {
    if (err) {
      console.log(err)
    } else {
      stripe.subscriptions.create(
        {
          customer: customer.id,
          items: [
            {
              plan
            }
          ]
        }, (err, subscription) => {
          if (err) {
            console.log(err)
          } else {
            res.status = 200
            res.send({
              status: 200,
              message: 'ok'
            })
          }
        }
      )
    }
  })
})

app.post('/mailers/shift-published', (req, res) => {
  const employee = req.body.employee
  ejs.renderFile('./views/emails/shiftPublished.ejs', { employee }, (err, htmlString) => {
    if (err) console.error(err)
    mg.messages.create('mail.toorno.io', {
      from: 'Andrea <andrea@toorno.io>',
      to: [employee.email],
      subject: 'You have been scheduled for your next shifts',
      html: htmlString
    })
      .then(msg => console.log('msg:', msg))
      .catch(err => console.log('err:', err))
  })
})

exports.api = functions.https.onRequest(app)

exports.inviteUser = functions.firestore
  .document('users/{userId}')
  .onCreate((snap, context) => {
    const employee = snap.data()
    if (employee.invited && !employee.invitationAccepted) {
      const url = `https://app.toorno.io/invitations/${employee.token}`
      ejs.renderFile('./views/emails/invitationEmail.ejs', { user: employee.name, organization: employee.organization, url }, (err, htmlString) => {
        if (err) console.error(err)
        return mg.messages.create('mail.toorno.io', {
          from: 'Andrea <andrea@toorno.io>',
          to: [employee.email],
          subject: 'Create your toorno account!',
          html: htmlString
        })
          .then(msg => console.log('msg:', msg)) // logs response data
          .catch(err => console.log('err:', err)) // logs any error
      }).catch((err) => console.log('err', err))
    }
    return true
  })
