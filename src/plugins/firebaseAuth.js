import store from '@/store'
import firebase from '@/firebase'
import db from '@/firebase/db'
const mixpanel = require('mixpanel-browser')

export default {
  install: (Vue, options) => {
    const auth = firebase.auth()
    Vue.prototype.$auth = {
      signup: async ({ organization, email, password, role }) => {
        await auth.createUserWithEmailAndPassword(email, password)
          .then((data) => {
            const organizationData = {
              name: organization,
              users: [email],
              setupComplete: false
            }

            const user = {
              organization,
              email,
              uid: data.user.uid
            }

            if (role === 'admin') user.admin = true

            db.collection('organizations').doc(organization).set(organizationData)
            db.collection('users').doc(user.email).set(user)
          })
      },
      retrieveInvitation: (invitation) => {
        return new Promise((resolve, reject) => {
          db.collection('users').doc(invitation).get()
            .then((userData) => {
              console.log(userData)
              if (userData.exists) {
                const user = userData.data()
                if (user.invited && !user.invitationAccepted) {
                  resolve(user)
                }
              } else {
                reject(new Error('The invitation is no longer valid'))
              }
            })
        })
      },
      createAccountAfterInvitation: async (user) => {
        await auth.createUserWithEmailAndPassword(user.email, user.password)
          .then((data) => {
            const { password, ...userWithNoPassword } = user
            userWithNoPassword.uid = data.user.uid
            db.collection('users').doc(userWithNoPassword.email).set(userWithNoPassword)
            return db.collection('organizations').doc(userWithNoPassword.organization).update({
              users: firebase.firestore.FieldValue.arrayUnion(userWithNoPassword.email)
            })
          })
          .then(() => {
            return db.collection('users').doc(user.token).delete()
          })
      },
      login: async ({ email, password }) => {
        await auth.signInWithEmailAndPassword(email, password)
      },
      logout: async () => {
        await auth.signOut()
      }
    }
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('uid', '==', user.uid).get()
          .then((data) => {
            const userData = data.docs[0]
            const user = userData.data()
            store.dispatch('setUser', user)
            mixpanel.identify(user.email)
            mixpanel.people.set({
              '$email': user.email,
              '$last_login': new Date(),
              'organization': user.organization,
              'role': user.admin ? 'admin' : user.manager ? 'manager' : 'employee'
            })
            return db.collection('organizations').doc(user.organization).get()
          })
          .then((organization) => {
            store.dispatch('setOrganization', organization.data())
            store.dispatch('getVacations')
            store.dispatch('getShifts')
            store.dispatch('getHourBankEntries')
            store.dispatch('getEmployees')
          })
      } else {
        store.dispatch('setUser', null)
        store.dispatch('setOrganization', null)
      }
    })
  }
}
