import '@babel/polyfill'
import Vue from 'vue'
import '@/plugins/vuetify'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import FirebaseAuthPlugin from '@/plugins/firebaseAuth'
import '@/assets/stylus/main.styl'
import VueStripeCheckout from 'vue-stripe-checkout'
import vueVimeoPlayer from 'vue-vimeo-player'

Vue.use(vueVimeoPlayer)
Vue.use(FirebaseAuthPlugin)
Vue.config.productionTip = false

const options = {
  key: process.env.VUE_APP_STRIPE_KEY,
  locale: 'auto',
  currency: 'EUR',
  panelLabel: 'Subscribe {{amount}}/month'
}

Vue.use(VueStripeCheckout, options)

window.mixpanel = require('mixpanel-browser')
window.mixpanel.init(process.env.VUE_APP_MIXPANEL_KEY)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
