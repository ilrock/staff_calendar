import Vue from 'vue'
import Router from 'vue-router'
import firebase from '@/firebase'

import Signup from '@/views/auth/Signup'
import Setup from '@/views/auth/setup/Setup'
import Login from '@/views/auth/Login'
import AcceptInvitation from '@/views/auth/AcceptInvitation'
import Calendar from '@/views/Calendar'

import Admin from '@/views/admin/Admin'
import Shifts from '@/views/admin/Shifts'
import Vacations from '@/views/admin/Vacations'
import MonthlyOverview from '@/views/admin/MonthlyOverview'
import Employees from '@/views/admin/Employees'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/setup',
      name: 'setup',
      component: Setup,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/employees',
      name: 'employees',
      component: Employees,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/invitations/:id',
      name: 'acceptInvitation',
      component: AcceptInvitation
    },
    {
      path: '/',
      name: 'calendar',
      component: Calendar,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/admin/shifts',
      name: 'admin/shifts',
      component: Shifts
    },
    {
      path: '/admin/vacations',
      name: 'admin/vacations',
      component: Vacations
    },
    {
      path: '/admin/monthly_overview',
      name: '/admin/monthly_overview',
      component: MonthlyOverview
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const currentUser = firebase.auth().currentUser
  if (requiresAuth && !currentUser) {
    next('/login')
  } else {
    next()
  }
})

export default router
