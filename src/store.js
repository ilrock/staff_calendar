import Vue from 'vue'
import Vuex from 'vuex'
import db from '@/firebase/db'
import moment from 'moment'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    organization: null,
    employees: [],
    shifts: [],
    vacations: [],
    hoursBank: []
  },
  getters: {
    user: (state) => state.user,
    organization: (state) => state.organization,
    employees: (state) => state.employees,
    shifts: (state) => {
      if (state.user) {
        if (state.user.admin || state.user.manager) {
          return state.shifts.map((shift) => ({
            ...shift,
            date: moment.unix(shift.date.seconds).format('YYYY-MM-DD')
          }))
        }

        return state.shifts.map((shift) => ({
          ...shift,
          date: moment.unix(shift.date.seconds).format('YYYY-MM-DD')
        })).filter((shift) => shift.published)
      }
    },
    vacations: (state) => {
      if (state.user) {
        if (state.user.admin || state.user.manager) {
          return state.vacations.map((vacation) => ({
            ...vacation,
            date: moment.unix(vacation.date.seconds).format('YYYY-MM-DD')
          }))
        }

        return state.vacations.map((vacation) => ({
          ...vacation,
          date: moment.unix(vacation.date.seconds).format('YYYY-MM-DD')
        })).filter((vacation) => vacation.confirmed)
      }
    },
    hoursBank: (state) => state.hoursBank
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    updateUser (state, payload) {
      state.user = payload
    },
    setOrganization (state, payload) {
      state.organization = payload
    },
    setEmployees (state, payload) {
      state.employees = payload
    },
    setShifts (state, payload) {
      state.shifts = payload
    },
    setVacations (state, payload) {
      state.vacations = payload
    },
    addToShifts (state, payload) {
      state.shifts = state.shifts.concat(payload)
    },
    setHoursBank (state, payload) {
      state.hoursBank = payload
    }
  },
  actions: {
    setUser ({ commit }, payload) {
      commit('setUser', payload)
    },
    updateUser ({ commit }, payload) {
      db.collection('users').doc(payload.email).update(payload)
        .then(() => commit('updateUser', payload))
    },
    setOrganization ({ commit }, payload) {
      commit('setOrganization', payload)
    },
    updateOrganization ({ commit, state }, payload) {
      return new Promise((resolve, reject) => {
        const organization = {
          ...state.organization,
          ...payload
        }

        db.collection('organizations').doc(state.organization.name).set(organization)
          .then(() => {
            commit('setOrganization', organization)
            resolve()
          })
      })
    },
    createEmployee ({ commit }, payload) {
      const crypto = require('crypto')
      const base64url = require('base64url')
      const token = base64url(crypto.randomBytes(20))

      const employee = {
        ...payload,
        invited: true,
        invitationAccepted: false,
        token
      }

      db.collection('users').doc(token).set(employee)
    },
    getEmployees ({ commit, state }) {
      db.collection('users').where('organization', '==', state.organization.name)
        .onSnapshot((snapshot) => {
          const employees = snapshot.docs.map(doc => doc.data())
          commit('setEmployees', employees)
        })
    },
    getShifts ({ commit, state }) {
      db.collection('shifts').where('organization', '==', state.organization.name)
        .onSnapshot((snapshot) => {
          const shifts = snapshot.docs.map((doc) => doc.data())
          commit('setShifts', shifts)
        })
    },
    getVacations ({ commit, state }) {
      db.collection('vacations').where('organization', '==', state.organization.name)
        .onSnapshot((snapshot) => {
          const vacations = snapshot.docs.map((doc) => doc.data())
          commit('setVacations', vacations)
        })
    },
    addShifts ({ commit }, payload) {
      payload.forEach(shiftData => {
        const momentDate = moment(shiftData.date).format('YYYY-MM-DD')
        const shift = {
          ...shiftData,
          employeeConfirmed: false,
          published: false,
          id: `${shiftData.employee.uid}_${momentDate}`
        }
        db.collection('shifts').doc(shift.id).set(shift)
      })

      // commit('addToShifts', payload)
    },
    addVacation ({ commit }, payload) {
      payload.forEach(vacationData => {
        const momentDate = moment(vacationData.date).format('YYYY-MM-DD')
        const vacation = {
          ...vacationData,
          confirmed: false,
          id: `${vacationData.employee.uid}_${momentDate}`
        }
        db.collection('vacations').doc(vacation.id).set(vacation)
      })
    },
    updateVacation ({ commit, state }, payload) {
      const vacation = state.vacations.find((vacation) => vacation.id === payload.id)

      console.log({ payload, state: state.vacations })

      return db.collection('vacations').doc(vacation.id).update({
        ...vacation,
        ...payload
      })
    },
    deleteVacation ({ commit }, { id }) {
      db.collection('vacations').doc(id).delete()
    },
    updateShift ({ dispatch, state }, payload) {
      console.log(payload)

      const shift = state.shifts.find((shift) => {
        return shift.id === payload.id
      })

      if (payload.employeeConfirmed) {
        dispatch('addToHoursBank', payload)
      }

      return db.collection('shifts').doc(shift.id).update({
        ...shift,
        ...payload
      })
    },
    deleteShift ({ commit }, { id }) {
      db.collection('shifts').doc(id).delete()
    },
    getHourBankEntries ({ commit, state }) {
      const startOfMonth = new Date(moment().startOf('month').format('YYYY-MM-DD'))
      const endOfMonth = new Date(moment().endOf('month').format('YYYY-MM-DD'))

      db.collection('hoursBank').where('date', '>', startOfMonth).where('date', '<', endOfMonth).get()
        .then((res) => {
          const entries = res.docs.map((doc) => doc.data())

          const groupedEntries = []
          const done = []
          entries.forEach(entry => {
            const employee = entry.employee
            if (done.indexOf(employee.uid) < 0) {
              const userEntries = entries.filter(el => el.employee.uid === employee.uid)

              let total = 0
              userEntries.forEach((userEntry) => { total += userEntry.totalTime })

              groupedEntries.push({
                employee,
                total
              })
              done.push(employee.uid)
            }
          })

          commit('setHoursBank', groupedEntries)
        })
    },
    addToHoursBank ({ commit }, payload) {
      const start = moment(payload.startTime, 'HH:mm')
      const finish = moment(payload.endTime, 'HH:mm')
      const diff = start.diff(finish)
      const diffDuration = Math.abs(moment.duration(diff).asMinutes())

      const id = moment(payload.date).format('YYYY-MM-DD')
      const entry = {
        id,
        date: payload.date,
        startTime: payload.startTime,
        endTime: payload.endTime,
        totalTime: diffDuration,
        employee: payload.employee
      }

      console.log(entry)

      db.collection('hoursBank').doc(entry.id).set(entry)
    }
  }
})
