import Api from '@/api'

const subscribeUser = (user) => Api().post('/subscriptions', user)

export {
  subscribeUser
}
