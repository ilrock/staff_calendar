import Api from '@/api'

const createInvitation = (employees) => Api().post('/invitations', employees)

export {
  createInvitation
}
