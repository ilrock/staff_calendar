import axios from 'axios'

export default() => {
  let baseURL
  if (process.env.NODE_ENV === 'development') {
    baseURL = '//localhost:5000/toorno-dev/us-central1/api'
  } else {
    baseURL = '//us-central1-toorno-6e34a.cloudfunctions.net/api'
  }

  return axios.create({
    withCredentials: false,
    baseURL,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
}
