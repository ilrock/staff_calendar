import Api from '@/api'

const sendShiftPublishedEmail = (employee) => Api().post('/mailers/shift-published', { employee })

export {
  sendShiftPublishedEmail
}
