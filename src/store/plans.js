const pricings = [
  {
    title: 'Paper Plane',
    slug: 'paper-plane',
    subtitle: 'Ideal for the smaller businesses',
    price: 1999,
    hover: false,
    active: false,
    features: [
      {
        icon: 'check_circle_outline',
        title: 'Up to 10 employees',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Up to 2 admins',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Count hours worked'
      },
      {
        icon: 'check_circle_outline',
        title: 'Count holidays earned'
      },
      {
        icon: 'check_circle_outline',
        title: 'Notifications system'
      }
    ]
  },
  {
    title: 'Airplane',
    slug: 'airplane',
    subtitle: 'Ideal for the medium businesses',
    price: 2999,
    hover: false,
    active: false,
    features: [
      {
        icon: 'check_circle_outline',
        title: 'Up to 25 employees',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Up to 5 admins',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Count hours worked'
      },
      {
        icon: 'check_circle_outline',
        title: 'Count holidays earned'
      },
      {
        icon: 'check_circle_outline',
        title: 'Notifications system'
      }
    ]
  },
  {
    title: 'Rocket',
    slug: 'rocket',
    subtitle: 'Ideal for the bigger businesses',
    price: 4999,
    hover: false,
    active: false,
    features: [
      {
        icon: 'check_circle_outline',
        title: 'Up to 50 employees',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Up to 10 admins',
        bold: true
      },
      {
        icon: 'check_circle_outline',
        title: 'Count hours worked'
      },
      {
        icon: 'check_circle_outline',
        title: 'Count holidays earned'
      },
      {
        icon: 'check_circle_outline',
        title: 'Notifications system'
      }
    ]
  }
]

export default pricings
